import React from 'react';
import Typical from 'react-typical';
export default function Profile() {
    return (
        <div className='profile-container'>
            <div className='profile-parent'>
               
                    <div className='colz'>
                    <a href='#'>
                        <i className='fa fa-facebook-square'></i>
                    </a>
                        <a href='#'>
                        <i className='fa fa-google-plus-square'></i>
                         </a>
                            <a href='#'>
                            <i className='fa fa-instagram'></i>
                            </a>
                                <a href='#'>
                                <i className='fa fa-youtube-square'></i>
                                </a>
                                    <a href='#'>
                                    <i className='fa fa-twitter'></i>
                                     </a>

                                     <div className='profile-details-name'>
                                    <span className='primary-text'>
                                    {" "}
                                    Hello,I'M <span className='hightlighted-text'>Ehiedu</span>

                                    </span>

                                    </div>
                    <div className='profile-details-role'>
                    <span className='primary-text'>
                        {" "}
                            <h1>
                            {" "} 
                            <Typical
                            loop={Infinity}
                            steps={[
                                1000,
                                "Full Stack Developer",
                                1000,
                                "Mern Stack Developer",
                                1000,
                                "Cross Platform Developer ",
                                1000,
                                "React/React Native Dev",
                                1000,
                            ]}
                            />
                            </h1>
                            <span className='profile-role-tagline'>
                                knack of building applications with frond and back endpoint
                                operation.
                            </span>
                        </span>
                        </div>
                            <div className='profiles-optins'>
                                <button className='btn primary-btn'>
                                {" "}
                                    hear Me {" "}
                                </button>
                            </div>
                </div>

            </div>
        </div>
    );
}

